#!/usr/bin/env bash

set -e

composer install --no-dev --no-interaction --no-ansi --no-progress --prefer-dist --no-scripts && \
    composer clear-cache --no-ansi --quiet

php bin/console app:collect-export-webcams

docker-php-entrypoint php-fpm
