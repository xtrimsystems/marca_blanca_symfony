<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twig\Environment;

class CollectAndExportWebcamsCommand extends Command
{
    protected static $defaultName = 'app:collect-export-webcams';

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @var Environment
     */
    private $twigEnvironment;

    /**
     * @var array
     */
    private $affiliates;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var mixed
     */
    private $webcamsApiUri;

    public function __construct(
        HttpClientInterface $client,
        Environment $twigEnvironment,
        ContainerBagInterface $params,
        Filesystem $filesystem,
        string $name = null
    ){
        parent::__construct($name);
        $this->client = $client;
        $this->twigEnvironment = $twigEnvironment;
        $this->affiliates = $params->get('affiliates');
        $this->webcamsApiUri = $params->get('webcamsUri');

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
        $this->filesystem = $filesystem;
    }

    protected function configure(): void
    {
        $this->setDescription('Collect & Export webcams')
            ->setHelp('Fetches a list of webcams and pre-renders the index.html files for all associates.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Collect & Export');

        $responseContent = $this->client->request('GET', $this->webcamsApiUri)->getContent();
        $webcams = $this->serializer->decode($responseContent, 'json');

        foreach ($this->affiliates as $key => $affiliate) {
            $this->filesystem->dumpFile(__DIR__.'/../../public/affiliates/'.$key.'/index.html', $this->twigEnvironment->render(
                'index.html.twig',
                [
                    'folderName' => $key,
                    'affiliate' => $affiliate,
                    'bigThumbnailsList' => $this->getBigThumbnailsList($webcams),
                    'smallThumbnailsList' => $this->getSmallThumbnailsList($webcams)
                ]
            ));

            $io->writeln(sprintf('Written index.html for %s', $key));
        }

        $io->success('Collect & Export Successful');

        return 0;
    }

    private function getBigThumbnailsList(array $webcams): array
    {
        $arr = [];

        if (sizeof($webcams) > 4) {
            for($i = 0; $i < 5; $i++) {
                $arr[] = $webcams[$i];
            }
        }

        return $arr;
    }

    private function getSmallThumbnailsList(array $webcams): array
    {
        $arr = [];
        $webcamsLength = sizeof($webcams);

        if ($webcamsLength > 1) {
            for($i = 1; $i < $webcamsLength; $i++) {
                $arr[] = $webcams[$i];
            }
        }

        return $arr;
    }
}
