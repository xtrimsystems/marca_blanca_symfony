<?php

namespace App\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('app');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->arrayNode('affiliates')
                ->prototype('array')
                    ->children()
                        ->scalarNode('name')->end()
                        ->scalarNode('url')->end()
                        ->scalarNode('nats_webcams')->end()
                        ->scalarNode('nats_website')->end()
                        ->scalarNode('assets_folder')->end()
                        ->scalarNode('analytics_code')->end()
                    ->end()
                ->end()
                ->end()
                ->arrayNode('apis')
                ->prototype('array')
                    ->children()
                        ->scalarNode('uri')
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
