#!/usr/bin/env bash

# Exit if a command exits with a non-zero status
set -e

NEW_UID=$(id -u)
NEW_GID=$(id -g)

# Global variables for docker
STR_1=$(printf "NEW_UID=%s\nNEW_GID=%s\n" "$NEW_UID" "$NEW_GID")

# Global variables for symfony
STR_2=$(printf "APP_ENV=%s\nAPP_SECRET=%s\n" "dev" "a7a9da112b47125aa8802a65e6b1af0d")

grep -qxF "$STR_1" .env || echo "$STR_1" >> .env
grep -qxF "$STR_2" .env || echo "$STR_2" >> .env

docker-compose up -d --build
