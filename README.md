# Marca Blanca
Same concept as in the [previous task](https://bitbucket.org/xtrimsystems/marca_blanca/src/master/README.md), but using symfony.

##### Description
Run a cronjob every 15 minutes that will execute the command `php bin/console app:collect-export-webcams`:

- Collects affiliates information (from a configuration file)
- Collects webcams information (from Cumlouder API)
- With the information collected, exports the affiliates index pages (index.html) into files in the respective public folders
- routes.yaml is configured to serve the static pages base on domain (at first I was using a Controller, but got so simple that using the TemplateController from symfony seems the best choice for such a simple job)

Finally, as the task describes, our affiliates needs to point their DNS to our server.

### Development / How to run
For local development you need docker.

Also edit your hosts file. Add the following line:
```
127.0.0.1       conejox.com cerdas.com babosas.biz
```

For the first time, we need to create the containers, run:
```
./setup.sh
```

That is all, it will create and start the containers, also execute the command to create the index.html files for each affiliate.

Now, open in your browser either [babosas.biz](babosas.biz) or [conejox.com](conejox.com) or [cerdas.com](cerdas.com)

If you see an error "502 Bad Gateway" give it some seconds/minute because it is getting installed vendors and the generation of the index.html files for each affiliate
##### Start/Stop containers
At anytime, to stop the containers run:
```
docker-compose stop
```

And to start again the containers run:
```
docker-compose up -d
```

##### Trigger Collect&Export 
On production we need to setup a cronjob that executes the command `php bin/console app:collect-export-webcams` every 15min in order to re-generate the index.html pages for all affiliates with the new webcams information.

On development, since we have symfony running inside a docker container, we need first to run it there, for that run:
```
docker exec marca_blanca_php_symfony php bin/console app:collect-export-webcams
```
