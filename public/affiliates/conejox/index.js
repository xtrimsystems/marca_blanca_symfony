window.onload = () => {
	const modal = document.querySelector('.modal');
	const closeModal = modal.querySelector('.close');
	const iframe = document.querySelector('.iframe');

	function showModal () {
		modal.classList.remove('isHidden');
	}

	function hideModal () {
		modal.classList.add('isHidden');
	}

	iframe.onload = showModal;
	closeModal.addEventListener('click', hideModal);
};
